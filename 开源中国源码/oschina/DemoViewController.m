//
//  DemoViewController.m
//  oschina
//
//  Created by lifei on 12-11-10.
//
//

#import "DemoViewController.h"
#import "AFOSCClient.h"
@interface DemoViewController ()

@end

@implementation DemoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[AFOSCClient sharedClient] getPath:@"https://api.weibo.com/oauth2/authorize"
                             parameters:
     [NSDictionary dictionaryWithObjectsAndKeys: nil]
                                success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                    
                                    NSLog(@"success");
                                    
                                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                    
                                    NSLog(@"failure");
                                    
                                }];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
